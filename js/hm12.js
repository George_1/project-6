[...inputs] = document.querySelectorAll("input");


inputs.forEach(inp => {
    inp.addEventListener("blur", (e) => {
        if (Number(e.target.value.length) === Number(e.target.dataset.length)) {
					inp.classList.add("green");
					inp.classList.remove("red");
				} else {
					inp.classList.remove("green");
					inp.classList.add("red");
				}
	});

	
});
