window.onload = () => {
    let hoursCnt = 0,
        minsCnt = 0,
        secsCnt = 0,
        runFlag = false,
        resetFlag = false,
        intervalCnt
    
        const displ = document.querySelectorAll(".stopwatch-display > span");
        displ[0].setAttribute("id", "hoursDisp");
        displ[1].setAttribute("id", "minsDisp");
        displ[2].setAttribute("id", "secsDisp");
    
    
        const btns = document.querySelectorAll(".stopwatch-control > button");
        btns[0].setAttribute("id", "startBtn");
        btns[1].setAttribute("id", "stopBtn");
        btns[2].setAttribute("id", "resetBtn");
    
        const cntr = document.querySelector(".container-stopwatch");
        const getData = (element) => {
            return document.getElementById(element);
        }
    
        getData("hoursDisp").innerText = `${hoursCnt}`.padStart(2, "0");  
        getData("minsDisp").innerText = `${minsCnt}`.padStart(2, "0"); 
        getData("secsDisp").innerText = `${secsCnt}`.padStart(2, "0");
    
    
    
    
        const counting = () => {
            
            secsCnt++;
    
            getData("secsDisp").innerText = `${secsCnt}`.padStart(2, "0");
            getData("minsDisp").innerText = `${minsCnt}`.padStart(2, "0");
            getData("hoursDisp").innerText = `${hoursCnt}`.padStart(2, "0");
            
    
            if (secsCnt === 60) {
                secsCnt = 0;
                getData("secsDisp").innerText = `${secsCnt}`.padStart(2, "0");
                minsCnt++;
                getData("minsDisp").innerText = `${minsCnt}`.padStart(2, "0");
            }
    
            if (minsCnt === 60) {
                minsCnt = 0;
                getData("minsDisp").innerText = `${minsCnt}`.padStart(2, "0");
                hoursCnt++;
                getData("secsDisp").innerText = `${secsCnt}`.padStart(2, "0");
            }
            
            if (hoursCnt === 60) {
                hoursCnt = 0;
                minsCnt = 0;
                secsCnt = 0;
                getData("secsDisp").innerText = `${secsCnt}`.padStart(2, "0");
                getData("minsDisp").innerText = `${minsCnt}`.padStart(2, "0");
                getData("hoursDisp").innerText = `${hoursCnt}`.padStart(2, "0");
        
            }
    
        }
        
        getData("startBtn").onclick = () => {
            
            if (!runFlag) {
                intervalCnt = setInterval(counting, 1000);
                runFlag = true;
                resetFlag = false;
               
            }
               cntr.classList.add("green");
            cntr.classList.remove("red");
            cntr.classList.remove("silver");
        }
    
        getData("stopBtn").onclick = () => {
        
            clearInterval(intervalCnt);
            runFlag = false;
            resetFlag = false;
            getData("secsDisp").innerText = `${secsCnt}`.padStart(2, "0");
            getData("minsDisp").innerText = `${minsCnt}`.padStart(2, "0");
            getData("hoursDisp").innerText = `${hoursCnt}`.padStart(2, "0");
            
            cntr.classList.add("red");
            cntr.classList.remove("green");
            cntr.classList.remove("silver");
            
        }
    
        getData("resetBtn").onclick = () => {
            if (!resetFlag) {
                clearInterval(intervalCnt);
            }
            cntr.classList.add("silver");
            cntr.classList.remove("green");
            cntr.classList.remove("red");
            
            resetFlag = true;
            runFlag = false;
            
            hoursCnt = 0;
            minsCnt = 0;
            secsCnt = 0;
            getData("secsDisp").innerText = `${secsCnt}`.padStart(2, "0");
            getData("minsDisp").innerText = `${minsCnt}`.padStart(2, "0");
            getData("hoursDisp").innerText = `${hoursCnt}`.padStart(2, "0");
    
        }
        
    
    
    
    }
    