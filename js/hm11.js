window.addEventListener("DOMContentLoaded", () => {

	let pattern1 = /[0-9]/,
		pattern2 = /\./;
	let enteredValue,
		enteredOperation,
		flagOperation = false,
		flagDigit = false,
		flagMemory = false,
		flagStart = false,
		digit = [],
		digit1 = '',
		digit2 = '',
		display='',
		result = '',
		mem = '';
    

	setId = elem => {
		switch (elem.className) {
			case "button black": {
				if (pattern1.test(elem.attributes[2].value)) {
					elem.id = `btn_${elem.attributes[2].value}`;
				} else {
					elem.id = `btn_dot`;
				}
				break;
			}
			case "button pink": {
				if (elem.attributes[2].value === "/") {
					elem.id = `btn_div`;
				} else if (elem.attributes[2].value === "*") {
					elem.id = `btn_mul`;
				} else if (elem.attributes[2].value === "-") {
					elem.id = `btn_min`;
				} else if (elem.attributes[2].value === "+") {
					elem.id = `btn_pls`;
				}
				break;
			}
			case "button gray": {
				if (elem.attributes[2].value === "mrc") {
					elem.id = `btn_mrc`;
				} else if (elem.attributes[2].value === "m+") {
					elem.id = `btn_mplus`;
				} else if (elem.attributes[2].value === "m-") {
					elem.id = `btn_mmin`;
				}
				break;
			}
			case "button orange": {
				elem.id = `btn_eqv`;
				
				break;
			}
		}
	};

	function calcs(dg1, dg2, oper) {
		switch (enteredOperation) {
			case "/": {
				result = digit1 / digit2;
				break;

			}
			case "*": {
				result = digit1 * digit2;
				break;
                           
			}
			case "-": {
				result = digit1 - digit2;
				break;
                           
			}
			case "+": {
				result = Number(digit1) + Number(digit2);
				break;
			}
                           
		}
		return result;
	}
        




	let buttons = document.querySelectorAll(".button");
	let test = document.getElementsByClassName("button orange");

	test[0].disabled = false;
	buttons.forEach(setId);

	let mLetter = document.createElement("div");
	mLetter.style.position = "absolute";
	mLetter.style.left = "10px";
	mLetter.style.top = "5px";
	mLetter.style.color = "black";
	mLetter.style.display = "none";
	mLetter.innerText = "m";
	mLetter.setAttribute("z-index", "10");
	document.querySelector(".display").append(mLetter);

    
	let disp = document.querySelector(".display input");
	disp.value = "";

	let inpt = addEventListener("click", (e) => {
		let presKey = e.target;
		if (presKey.type === 'button') {
	
			disp.value = "";
			switch (presKey.value) {
				case "0":
				case "1":
				case "2":
				case "3":
				case "4":
				case "5":
				case "6":
				case "7":
				case "8":
				case "9":
				case ".": {
		
					enteredValue = presKey.value;
					if (pattern2.test(digit.join("")) && enteredValue === ".") {
						enteredValue = "";
						display = digit.join("");
						disp.value = display;
						break;
					}

					digit.push(enteredValue);
					display = digit.join("");
					disp.value = display;

					flagDigit = true;

					break;
				}

				case "C": {
					flagDigit = false;
					flagOperation = false;
					flagEqual = false;
					enteredValue = "";
					digit = [];
					digit1 = "";
					digit2 = "";
					display = '';
					disp.value = display;

					break;
				}

				case "/":
				case "*":
				case "+":
				case "-":
				case "=": {
					debugger;
					if (flagDigit) {
						if (enteredOperation === '=' && presKey.value === '=') {
						disp.value = display;
						break;
						}

						if (enteredOperation === '=' && presKey.value !== '=') {
							enteredOperation = presKey.value;
							disp.value = display;
							digit = [];
							flagOperation = true;
							break;
						}

						if (!flagOperation) {
							debugger;
							enteredOperation = presKey.value;
							display = digit.join("");
							disp.value = display;
							digit1 = Number(display);
							digit = [];
							flagOperation = true;
						} else {
							if (digit.length > 0) {
								debugger;
								display =digit.join("");
								disp.value = display;
								digit2 = Number(display);

								result = calcs(digit1, digit2, enteredOperation);

								display = result;
								disp.value = display;

								digit1 = display;
								enteredOperation = presKey.value;
								digit2 = "";
								digit = [];
								if (presKey.value === "=") {
									flagOperation = false;
								} else {
									flagOperation = true;
									flagDigit = true;
								}
							} else {
								debugger;
								enteredOperation = presKey.value;
								disp.value = display;
								flagOperation = true;
							}
						}
					} else {
						debugger
						disp.value = display; 
					}

					

					break;
				}


				case "mrc": {
					if (flagMemory) {
						disp.value = mem;
						digit1 = mem;
						flagMemory = false;
					} else {
						mem = "";
						display = '';
						disp.value = "";
						digit1 = "";
						flagDigit = false;
						mLetter.style.display = "none";
					}

					break;
				}
				case "m+":
				case "m-": {
					mem = display;
					mLetter.style.display = "block";
					disp.value = display;
					flagMemory = true;

					break;
				}
			}
		}
		})



});


