window.onload = ()  => {

    let inputTel = document.createElement("input");
    inputTel.setAttribute("type", "text");
    inputTel.setAttribute("placeholder", "Введить номер телефону у форматі 000-000-00-00");
    inputTel.setAttribute("id", "input");

    let drawButton = document.createElement("input");
    drawButton.setAttribute("type", "button");
    drawButton.setAttribute("value", "Зберегти");
    drawButton.setAttribute("id", "draw-button");
    
    const divA = document.querySelector(".out");
    divA.append(inputTel);
    divA.append(drawButton);


    drawButton.onclick = () => {
        console.dir(inputTel.value);
        let inpt1 = inputTel.value;
        let pattern1 = new RegExp("\\d\\d\\d-\\d\\d\\d-\\d\\d-\\d\\d");
        let result = pattern1.test(inputTel.value);
        if (result) {
            inputTel.classList.add("green");
        } else {
            inputTel.classList.remove("green");
            let errString= `<div> Помилка ввода номера по шаблону 000-000-00-00 </div>`
            inputTel.insertAdjacentHTML("beforebegin", errString);
            let errDiv = inputTel.parentElement.childNodes[1];
            errDiv.classList.add("red");

        }

    };


}
