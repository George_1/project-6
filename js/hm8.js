document.querySelector(".circle_button").onclick = function () {
	
	let begins = document.querySelector(".circle_button");

	let inp1 = document.createElement("input");
	inp1.type = "number";
	inp1.name = "diametr";
	inp1.value = 50;
	inp1.min = 20;
	inp1.max = 1000;
	begins.after(inp1);

	let but2 = document.createElement("input");
	but2.type = "button";
	but2.value = "Намалювати";
	
	but2.classList.add("div_all");
	inp1.after(but2);

	but2.onclick = function () {
		let diam = `${inp1.value}px`;
		let circlArr = [];
		var div2 = document.createElement("div");
		div2.style.display = "flex";
		div2.style.flexDirection = "column";
		div2.classList.add("div2");
		but2.after(div2);
	
		for (let i = 1; i <= 10; i++) {
			let circl1 = document.createElement("div");
			circl1.style.display = "inline-block";
			circl1.classList.add("circl1");
			
			for (j = 1; j <= 10; j++) {

				let circl = document.createElement("div");
                
				circl.classList.add("circl");
				circl.style.textAlign = "center";
				circl.style.width = diam;
				circl.style.height = diam;

				let clr = Math.floor(Math.random() * 360);

				circl.style.backgroundColor = `hsl(${clr}, 50%, 50%)`;
				circl.style.border = `1px solid hsl(${clr}, 50%, 50%)`;
				circl.style.borderRadius = "50%";
				circl.style.display = "inline-block";
				circl.innerText = `${(i - 1) * 10 + j}`;
				circl.setAttribute("id", `${(i-1) * 10 + j}`);
				circl.onclick = removeCircle;
				circlArr.push(circl);
				circl1.append(circl);
			}
			div2.append(circl1);
		}



		function removeCircle() {
			this.remove();
			circlArr.splice(this.getAttribute("id") - 1, 1);
		
			for (i = 0; i < circlArr.length; i++){
				circlArr[i].id = i+1;
			}
			
			div2.innerHTML = "";
			
			for (let j = 0; j <= circlArr.length / 10 + 1; j++) {
				let circl1 = document.createElement("div");
				circl1.append(...circlArr.slice(j * 10, j * 10 + 10));
				div2.append(circl1);
			}

			
		}
	};
};

